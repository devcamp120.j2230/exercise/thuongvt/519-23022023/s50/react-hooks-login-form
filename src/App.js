import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import HooksLoginForm from './component/hooksLoginForm';

function App() {
  return (
    <div>
      <HooksLoginForm></HooksLoginForm>
    </div>
  );
}

export default App;
